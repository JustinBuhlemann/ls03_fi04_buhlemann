package view.menue;

import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.AlienDefenceController;
import model.User;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;

public class UserWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField tfFirstName;
	private JTextField tfLastName;
	private JTextField tfStreet;
	private JTextField tfBirthdate;
	private JTextField tfHousenumber;
	private JTextField tfPostal;
	private JTextField tfCity;
	private JTextField tfLogin;
	private JTextField tfPassword;
	private JTextField tfSalary;
	private AlienDefenceController alienDefenceController;
	private JTextField tfMarital;
	private JTextField tfGrade;

	public UserWindow(AlienDefenceController alienDefenceController) {
		getContentPane().setBackground(Color.BLACK);
		setBackground(Color.BLACK);
		
		this.alienDefenceController = alienDefenceController;
		
		setBounds(100, 100, 406, 406);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Vorname:");
		lblNewLabel.setForeground(Color.YELLOW);
		lblNewLabel.setBounds(10, 11, 46, 14);
		this.getContentPane().add(lblNewLabel);
		
		JLabel lblNachname = new JLabel("Nachname:");
		lblNachname.setForeground(Color.YELLOW);
		lblNachname.setBounds(10, 36, 66, 14);
		this.getContentPane().add(lblNachname);
		
		JLabel lblNewLabel_1_1 = new JLabel("Geburtstag:");
		lblNewLabel_1_1.setForeground(Color.YELLOW);
		lblNewLabel_1_1.setBounds(10, 61, 73, 14);
		this.getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Stra\u00DFe:");
		lblNewLabel_1_1_1.setForeground(Color.YELLOW);
		lblNewLabel_1_1_1.setBounds(10, 86, 46, 14);
		this.getContentPane().add(lblNewLabel_1_1_1);
		
		JLabel lblNewLabel_1_1_2 = new JLabel("Hausnummer:");
		lblNewLabel_1_1_2.setForeground(Color.YELLOW);
		lblNewLabel_1_1_2.setBounds(10, 111, 73, 14);
		this.getContentPane().add(lblNewLabel_1_1_2);
		
		JLabel lblNewLabel_1_1_3 = new JLabel("Postleitzahl:");
		lblNewLabel_1_1_3.setForeground(Color.YELLOW);
		lblNewLabel_1_1_3.setBounds(10, 136, 73, 14);
		this.getContentPane().add(lblNewLabel_1_1_3);
		
		JLabel lblNewLabel_1_1_4 = new JLabel("Stadt:");
		lblNewLabel_1_1_4.setForeground(Color.YELLOW);
		lblNewLabel_1_1_4.setBounds(10, 161, 46, 14);
		this.getContentPane().add(lblNewLabel_1_1_4);
		
		JLabel lblNewLabel_1_1_4_1 = new JLabel("Login:");
		lblNewLabel_1_1_4_1.setForeground(Color.YELLOW);
		lblNewLabel_1_1_4_1.setBounds(10, 186, 46, 14);
		this.getContentPane().add(lblNewLabel_1_1_4_1);
		
		JLabel lblNewLabel_1_1_4_2 = new JLabel("Passwort:");
		lblNewLabel_1_1_4_2.setForeground(Color.YELLOW);
		lblNewLabel_1_1_4_2.setBounds(10, 211, 66, 14);
		this.getContentPane().add(lblNewLabel_1_1_4_2);
		
		JLabel lblNewLabel_1_1_4_3 = new JLabel("Gehalt:");
		lblNewLabel_1_1_4_3.setForeground(Color.YELLOW);
		lblNewLabel_1_1_4_3.setBounds(10, 236, 46, 14);
		this.getContentPane().add(lblNewLabel_1_1_4_3);
		
		JLabel lblNewLabel_1_1_4_4 = new JLabel("Beziehungsstatus:");
		lblNewLabel_1_1_4_4.setForeground(Color.YELLOW);
		lblNewLabel_1_1_4_4.setBounds(10, 261, 95, 14);
		this.getContentPane().add(lblNewLabel_1_1_4_4);
		
		JLabel lblNewLabel_1_1_4_5 = new JLabel("Abschlussnote:");
		lblNewLabel_1_1_4_5.setForeground(Color.YELLOW);
		lblNewLabel_1_1_4_5.setBounds(10, 286, 95, 14);
		this.getContentPane().add(lblNewLabel_1_1_4_5);
		
		tfFirstName = new JTextField();
		tfFirstName.setBounds(115, 11, 256, 20);
		this.getContentPane().add(tfFirstName);
		tfFirstName.setColumns(10);
		
		tfLastName = new JTextField();
		tfLastName.setColumns(10);
		tfLastName.setBounds(115, 36, 256, 20);
		this.getContentPane().add(tfLastName);
		
		tfStreet = new JTextField();
		tfStreet.setColumns(10);
		tfStreet.setBounds(115, 86, 256, 20);
		this.getContentPane().add(tfStreet);
		
		tfBirthdate = new JTextField();
		tfBirthdate.setColumns(10);
		tfBirthdate.setBounds(115, 61, 256, 20);
		this.getContentPane().add(tfBirthdate);
		
		tfHousenumber = new JTextField();
		tfHousenumber.setColumns(10);
		tfHousenumber.setBounds(115, 111, 256, 20);
		this.getContentPane().add(tfHousenumber);
		
		tfPostal = new JTextField();
		tfPostal.setColumns(10);
		tfPostal.setBounds(115, 136, 256, 20);
		this.getContentPane().add(tfPostal);
		
		tfCity = new JTextField();
		tfCity.setColumns(10);
		tfCity.setBounds(115, 161, 256, 20);
		this.getContentPane().add(tfCity);
		
		tfLogin = new JTextField();
		tfLogin.setColumns(10);
		tfLogin.setBounds(115, 186, 256, 20);
		this.getContentPane().add(tfLogin);
		
		tfPassword = new JTextField();
		tfPassword.setColumns(10);
		tfPassword.setBounds(115, 211, 256, 20);
		this.getContentPane().add(tfPassword);
		
		tfSalary = new JTextField();
		tfSalary.setColumns(10);
		tfSalary.setBounds(115, 236, 256, 20);
		this.getContentPane().add(tfSalary);
		
		tfMarital = new JTextField();
		tfMarital.setColumns(10);
		tfMarital.setBounds(115, 258, 256, 20);
		getContentPane().add(tfMarital);
		
		tfGrade = new JTextField();
		tfGrade.setColumns(10);
		tfGrade.setBounds(115, 283, 256, 20);
		getContentPane().add(tfGrade);
		
		JButton btnCreateUser = new JButton("Benutzer erstellen");
		btnCreateUser.setBounds(215, 333, 156, 23);
		btnCreateUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CreateUser();
			}
		});
		getContentPane().add(btnCreateUser);
		
		this.setVisible(true);
	}
	
	private void CreateUser() {
		if (tfFirstName.getText() == "" || tfLastName.getText() == "" || tfStreet.getText() == "" || tfHousenumber.getText() == "" || tfPostal.getText() == "" || tfCity.getText() == "" || 
				tfLogin.getText() == "" || tfPassword.getText() == "" || tfSalary.getText() == "" || tfMarital.getText() == "" || tfGrade.getText() == "" || tfBirthdate.getText() == "") {
			return;
		}
		
		int salary;
		double finalGrade;
		LocalDate birthdate;
		
		try {
			salary = Integer.parseInt(tfSalary.getText());
	    } catch (NumberFormatException e) {
	        return;
	    }
		
		try {
			finalGrade = Double.parseDouble(tfGrade.getText());
	    } catch (NumberFormatException e) {
	        return;
	    }
		
		try {
			birthdate = LocalDate.parse(tfBirthdate.getText());
	    } catch (NumberFormatException e) {
	        return;
	    }
		
		User newUser = new User(0, tfFirstName.getText(), tfLastName.getText(), birthdate, tfStreet.getText(),
				tfHousenumber.getText(), tfPostal.getText(), tfCity.getText(), tfLogin.getText(), tfPassword.getText(), salary,
				tfMarital.getText(), finalGrade);
		
		alienDefenceController.getUserController().createUser(newUser);
		dispose();
	}
}
