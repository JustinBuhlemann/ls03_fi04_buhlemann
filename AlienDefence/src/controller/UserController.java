package controller;

import model.User;
import model.persistance.IPersistance;
import model.persistance.IUserPersistance;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IPersistance alienDefenceModel) {
		this.userPersistance = alienDefenceModel.getUserPersistance();
	}
	
	public void createUser(User user) {
		if(this.userPersistance.readUser(user.getLoginname()) == null) {
            this.userPersistance.createUser(user);
        }
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		User user = userPersistance.readUser(username);
		return user;
	}
	
	public void changeUser(User user) {
		userPersistance.updateUser(user);
	}
	
	public void deleteUser(User user) {
		userPersistance.deleteUser(user.getP_user_id());
	}
	
	public boolean checkPassword(String username, String passwort) {
		User user = userPersistance.readUser(username);
		
		return user.getPassword().equals(passwort);
	}
}
