package formAendern;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class formAendernGUI {

	private JFrame frame;
	private JTextField tfdNewText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					formAendernGUI window = new formAendernGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public formAendernGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 422, 556);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblText = new JLabel("Hier bitte Text eingeben");
		lblText.setBounds(10, 11, 386, 40);
		lblText.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblText);
		
		JLabel lblAufgabe1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabe1.setBounds(10, 51, 416, 19);
		frame.getContentPane().add(lblAufgabe1);
		
		JButton btnBackgroundColorRed = new JButton("Rot");
		btnBackgroundColorRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().setBackground(new Color(255, 0, 0));
			}
		});
		btnBackgroundColorRed.setBounds(10, 81, 89, 23);
		frame.getContentPane().add(btnBackgroundColorRed);
		
		JButton btnBackgroundColorGreen = new JButton("Gr\u00FCn");
		btnBackgroundColorGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().setBackground(new Color(0, 255, 0));
			}
		});
		btnBackgroundColorGreen.setBounds(109, 81, 89, 23);
		frame.getContentPane().add(btnBackgroundColorGreen);
		
		JButton btnBackgroundColorYellow = new JButton("Gelb");
		btnBackgroundColorYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().setBackground(new Color(255, 255, 0));
			}
		});
		btnBackgroundColorYellow.setBounds(307, 81, 89, 23);
		frame.getContentPane().add(btnBackgroundColorYellow);
		
		JButton btnBackgroundColorBlue = new JButton("Blau");
		btnBackgroundColorBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().setBackground(new Color(0, 0, 255));
			}
		});
		btnBackgroundColorBlue.setBounds(208, 81, 89, 23);
		frame.getContentPane().add(btnBackgroundColorBlue);
		
		JButton btnBackgroundColorStandard = new JButton("Standardfarbe");
		btnBackgroundColorStandard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().setBackground(null);
			}
		});
		btnBackgroundColorStandard.setBounds(10, 116, 164, 23);
		frame.getContentPane().add(btnBackgroundColorStandard);
		
		JButton btnBackgroundColorSelect = new JButton("Farbe w\u00E4hlen");
		btnBackgroundColorSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JColorChooser colorChooser = new JColorChooser();
				Color newColor = colorChooser.showDialog(null, "Farbe ausw�hlen", null);
				frame.getContentPane().setBackground(newColor);
			}
		});
		btnBackgroundColorSelect.setBounds(184, 115, 212, 23);
		frame.getContentPane().add(btnBackgroundColorSelect);
		
		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(10, 146, 416, 19);
		frame.getContentPane().add(lblAufgabe2);
		
		JButton btnFontArial = new JButton("Arial");
		btnFontArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Arial", 0, 11));
			}
		});
		btnFontArial.setBounds(10, 176, 114, 23);
		frame.getContentPane().add(btnFontArial);
		
		JButton btnFontComicSans = new JButton("Comic Sans MS");
		btnFontComicSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Comic Sans MS", 0, 11));
			}
		});
		btnFontComicSans.setBounds(146, 176, 114, 23);
		frame.getContentPane().add(btnFontComicSans);
		
		JButton btnFontCourier = new JButton("Courier New");
		btnFontCourier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Courier New", 0, 11));
			}
		});
		btnFontCourier.setBounds(282, 176, 114, 23);
		frame.getContentPane().add(btnFontCourier);
		
		tfdNewText = new JTextField();
		tfdNewText.setBounds(10, 210, 386, 20);
		frame.getContentPane().add(tfdNewText);
		tfdNewText.setColumns(10);
		
		JButton btnApplyText = new JButton("Text \u00FCbernehmen");
		btnApplyText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setText(tfdNewText.getText());
			}
		});
		btnApplyText.setBounds(10, 236, 157, 23);
		frame.getContentPane().add(btnApplyText);
		
		JButton btnClearText = new JButton("Text l\u00F6schen");
		btnClearText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setText("");
			}
		});
		btnClearText.setBounds(177, 236, 114, 23);
		frame.getContentPane().add(btnClearText);
		
		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setBounds(10, 270, 416, 19);
		frame.getContentPane().add(lblAufgabe3);
		
		JButton btnFontRed = new JButton("Rot");
		btnFontRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(new Color(255, 0, 0));
			}
		});
		btnFontRed.setBounds(10, 300, 114, 23);
		frame.getContentPane().add(btnFontRed);
		
		JButton btnFontBlack = new JButton("Schwarz");
		btnFontBlack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(new Color(0, 0, 0));
			}
		});
		btnFontBlack.setBounds(282, 300, 114, 23);
		frame.getContentPane().add(btnFontBlack);
		
		JButton btnFontBlue = new JButton("Blau");
		btnFontBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(new Color(0, 0, 255));
			}
		});
		btnFontBlue.setBounds(146, 300, 114, 23);
		frame.getContentPane().add(btnFontBlue);
		
		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabe4.setBounds(10, 330, 416, 19);
		frame.getContentPane().add(lblAufgabe4);
		
		JButton btnFontBigger = new JButton("+");
		btnFontBigger.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font(lblText.getFont().getFontName(), 0, lblText.getFont().getSize() + 1));
			}
		});
		btnFontBigger.setBounds(10, 360, 157, 23);
		frame.getContentPane().add(btnFontBigger);
		
		JButton btnFontLower = new JButton("-");
		btnFontLower.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font(lblText.getFont().getFontName(), 0, lblText.getFont().getSize() - 1));
			}
		});
		btnFontLower.setBounds(190, 360, 157, 23);
		frame.getContentPane().add(btnFontLower);
		
		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setBounds(10, 394, 416, 19);
		frame.getContentPane().add(lblAufgabe5);
		
		JButton btnLabelLeft = new JButton("links");
		btnLabelLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(2);
			}
		});
		btnLabelLeft.setBounds(10, 424, 114, 23);
		frame.getContentPane().add(btnLabelLeft);
		
		JButton btnLabelCenter = new JButton("zentriert");
		btnLabelCenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(0);
			}
		});
		btnLabelCenter.setBounds(146, 424, 114, 23);
		frame.getContentPane().add(btnLabelCenter);
		
		JButton btnLabelRight = new JButton("rechts");
		btnLabelRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(4);
			}
		});
		btnLabelRight.setBounds(282, 424, 114, 23);
		frame.getContentPane().add(btnLabelRight);
		
		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm.setBounds(10, 453, 416, 19);
		frame.getContentPane().add(lblAufgabeProgramm);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(10, 483, 386, 23);
		frame.getContentPane().add(btnExit);
	}
}
