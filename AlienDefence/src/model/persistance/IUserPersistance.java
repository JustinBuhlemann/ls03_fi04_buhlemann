package model.persistance;

import java.util.List;

import model.Target;
import model.User;

public interface IUserPersistance {
	
	int createUser(User user);

	User readUser(String username);

	void updateUser(User user);

	void deleteUser(int user_id);

}